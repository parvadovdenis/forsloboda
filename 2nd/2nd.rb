require './city.rb'
require './deixtra.rb'

show_exapmle = true
pathes = []

print "Enter number of tests"
testCount = gets.to_i

testCount.times do |testNumber|
  testNumber = testNumber + 1
  puts "Information for test №#{testNumber}"


  print "Enter number of cities"
  citiesCount = gets.to_i

  puts ''
  puts "Cities:"
  puts ''

  citiesCount.times do |cityIndex|
    index = cityIndex + 1
    print "City name №#{index}: "

    name = gets.chomp
    city = City.new(index, name)

    print "Enter number of cities in the neighboring city for  №#{city.id} #{city.name}: "
    countOfNeighbours = gets.to_i

    if show_exapmle
      puts ''
      puts "instructions:"
      puts "Enter the ID of the neighbor and the city-cost path to it"
      puts "example:"
      puts "2 10"
      puts ''
      show_exapmle = false
    end

    puts "Entry  the neighbors of the city#{city.name}"

    countOfNeighbours.times do |i|
      print "enter the ID and the cost for the city-neighbor №#{i + 1}: "
      params = gets
      params = params.split(' ')
      city.add_road(params.first.to_i, params.last.to_i)
    end
  end

  puts "Finish"

  City.all.each do |c|
    c.print
  end

  print "How many ways must be found ?:"
  pathCount = gets.to_i
  puts "Enter the start and end points through the gap"

  pathCount.times do |i|
    print "Way №#{i+1}: "
    params = gets
    params = params.split(' ')
    pathes[i] = [params[0], params[1]]
  end

  d = Deixtra.new
  City.all.each do |city|
    d.add_vertex(Deixtra::Vertex.new city)
  end

  pathes.each do |path|
    from = City.find_by_name(path[0])
    to = City.find_by_name(path[1])
    path_length = d.process from.id, to.id
    puts "Distance from #{from.name} to #{to.name} - #{path_length}"
    d.reset
  end

  show_exapmle = true
  pathes = []
  City.reset
  puts ''
end





