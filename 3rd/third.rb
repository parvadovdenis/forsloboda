def factorial(n)
  if n == 0
    1
  else
    n * factorial(n - 1)
  end
end


def sum_of_digits(x, q=10)
  res = 0
  while x > 0
    res += x % q
    x /= q
  end
  res
end

print(sum_of_digits(factorial(100)))