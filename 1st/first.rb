class FirstTask
  # def initialize
  #   @nums = [1]
  # end

  def [](idx)
    @nums = [1]
    @nums[idx] ||= calc(idx)
  end

  private
  def calc(idx)
    sum = 0
    idx.times do |i|
      sum += self[i] * self[idx - 1 - i]
    end
    sum
  end
end

print(FirstTask.new[gets.chomp.to_i])

